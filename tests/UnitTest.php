<?php

namespace App\Tests;

use App\Entity\Client;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testClient(): void
    {
        $client = new Client();
        $client->setCin('12345678');
        $client->setNom('Raouia');
        $client->setPrenom('Ben nsib');
        $client->setAdresse('Madina');
        $this->assertSame('12345678', $client->getCin());
        $this->assertSame('Raouia', $client->getNom());
        $this->assertSame('Ben nsib', $client->getPrenom());
        $this->assertSame('Madina', $client->getAdresse());
    }
}